# Sample golang aproject app

This sample app project is combining the structure inspired by <https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/> , demo sugested at <https://github.com/katzien/go-structure-examples/tree/master/domain-hex-actor>, previous engineering experiences in RevEng and PriSec.

This serve as an example project structuring approach for Edwin as the DRI to easily follow and review code of products.

The design of this app can always be changed as long as meaningful suggestion is made through opening Issues in the repo.

## Software architecturing in Go

Before you go through this section, make sure you have understood the article <https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/>

Here we take the following concepts from the article

```TODO```

### Example
```bash
└── internal
    ├── adapter
    │   ├── _adapter_a_
    │   │   └── _component_a_.go
    │   └── _adapter_b_
    │       ├── _component_a_.go
    │       └── _component_b_.go
    ├── app
    │   ├── _app_a_
    │   │   ├── _command_handler_a_.go
    │   │   └── endpoint
    │   │       └── rpc
    │   │           └── grpc #this layer can have sub-folder as well.
    │   │               └── _grpc_handler_a_.go
    │   └── _app_b_
    │       ├── cli
    │       │   └── _cli_command_b_.go
    │       └── _command_handler_b_.go
    ├── core
    │   ├── _component_a_
    │   │   │   └── _compoment_c_ # use the _component a namespace but not subpackage of component a
    │   │   │       └── c.go
    │   │   ├── adapter.go #define the interface of adapter it need
    │   │   ├── _a_.go #define the model
    │   │   ├── _foo_.go #special model/groupping of logics that are related to the domain. Like Auth in Authentik
    │   │   └── service.go #application service of the component
    │   └── _component_b_
    │       ├── adapter.go
    │       ├── b.go
    │       ├── event.go #groupped event related logic of the component
    │       └── service.go
    └── pkg
        ├── _groupped_namespace_
        │   └── _shared_pkg_c_
        │       └── _pkg_code_.go
        ├── _shared_pkg_a_
        │   └── _pkg_code_.go
        └── _shared_pkg_b_
            └── _pkg_code_.go
```

### /internal

This is the layer where we put all the actual code of the project

### /internal/adapter

This is the layer for the implementation of adapters to use the infrastructure.(`Secondary or Driven Adapters`) Each infracture is a package. (e.g. mariadb, elasticsearch, redis...etc)

### /internal/app

This is the layer for puting app related logic. 

It includes

1. Command handler
1. API implementation
1. Init logic of creating instances of compoments, adapters and inject into appropriate objects.

### /internal/core

This is the layer for application core. It follows pack by component methodology to pack things together. Each component is a package.

A package of core layer defines:

1. Domain Model
1. Domain Logic
1. Application Service

### /internal/pkg

This is the layer for shared packages across the project.

## Package inside Package?

It is possible to have package inside package. Since Go does not have the concept of sub-package. A package inside another package is just using the namespace of the parent's package.

### Example

A platform that allow users to view beers and give reviews.

There will be two components

1. Beer
1. Beer Review

in this case, the structure will look like

```bash
├── internal
│   ├── core
│   │   ├── beer # the beer component
│   │   │   ├── adapter.go
│   │   │   ├── beer.go
│   │   │   ├── event.go
│   │   │   └── review # the beer review component
```
