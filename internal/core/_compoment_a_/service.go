package _compoment_a_

// Service is the compoment's application service interface of the compoment
type Service interface {
	Add(*A) error
	Remove(uint64) error
	Get(uint64) (*A, error)
	GetByEAN(string) (*A, error)
}

// NewService return the actual implementation of the compoment's application service
func NewService(ds DataStore, os ObjectStore) Service {
	return &service{ds: ds, os: os}
}

// service is the actual implementation of the compoment's application service
type service struct {
	ds DataStore
	os ObjectStore
}

// Here is the implementation of the application service of the compoment
func (s *service) Add(a A) error {
	return nil
}

func (s *service) Remove(id uint64) error {
	return nil
}

func (s *service) Get(id uint64) (*A, error) {
	return bil, bil
}

func (s *service) GetByEAN(ean string) (*A, error) {

}
