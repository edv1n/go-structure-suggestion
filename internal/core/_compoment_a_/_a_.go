package _compoment_a_

// A is the domain model of the compoment, naming according to the name of the compoment
type A struct {
	Name string // field of the model A
	EAN  string // field of the model A
}

// Create is a domain service of the compoment
func (a *A) Create() {

}
