package _compoment_a_

import (
	"_project_full_go_path/internal/pkg/storage"
)

// here we define what kind of intrastrucutre the compoment need to use. (the interface of the adapter it need.)

type DataStore interface {
	Insert(*A) (*A, error)
	Update(*A) (*A, error)
	// If the return value is nil, means it is not found.
	// If the return value is not nil, means an record has been retrieved
	// Error means an error has occur when looking for the record
	Query(uint64) (*A, error)
	// It will return a list of records that match the given criteria
	ListByName(string) ([]A, error)
	// Another way to perform the listing.
	// This time by the Brewery
	QueryByEAN(string) (*A, error)
	Delete(id uint64) error
}

// Objectstore is an interfaces that a object file store adopter has to fulfill
// The compoment will assume it has full control of the Object Store given
type ObjectStore interface {
	// Since it is very common to have the same specification of using object storage, it directly use the general object store adapter interface here
	storage.ObjectStore
}
