package _compoment_b_

// StreamProvider is an interface a stream provider (PubSub) has to fulfill
type StreamProvider interface {
	Subscribe(o B) (chan Event, error)
	Publish(e Event) error
}
