package _compoment_b_

// Service is the compoment's application service interface of the compoment
type Service interface {
	Add(*B) error
	Remove(uint64) error
	Get(uint64) (*B, error)
	GetByEAN(string) (*B, error)
	SubscribeEvent(uint64)
}

// NewService return the actual implementation of the compoment's application service
func NewService(ds DataStore, os ObjectStore) Service {
	return &service{ds: ds, os: os}
}

// service is the actual implementation of the compoment's application service
type service struct {
	ds DataStore
	os ObjectStore
}

// Here is the implementation of the application service of the compoment
func (s *service) Add(a A) error {
	return nil
}

func (s *service) Remove(id uint64) error {
	return nil
}

func (s *service) Get(id uint64) (*B, error) {
	return bil, bil
}

func (s *service) GetByEAN(ean string) (*B, error) {

}
