package _compoment_b_

// the domain model of compoment b
type B struct {
}

// domain service of the domain model b
func (b *B) Play() {}
