package _adapter_a_

import (
	"peoject_full_path/internal/component/_component_a_"
)

type _component_a_ObjectStore struct {
}

// As a checker to see if have implemented all required interfaces
var _ _component_a_.ObjectStore = &_component_a_ObjectStore{}

// implement the adapter defined by _compoment_a_ with this choosen infrastructure tool
func (a *_component_a_ObjectStore) Insert(*A) (*A, error) {

}
