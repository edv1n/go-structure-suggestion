package _app_a_

// an empty struct for groupping command handlers
type _command_handler_a_ struct {
}

func (_ _command_handler_a_) SomeCrazyBusinessLogicRelatedToRpcCall(arg1 string, arg2 int) {
	// here you call initialized compoments and use them to perform the handler logic
}
